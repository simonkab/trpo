package skin.expert.mobile

import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import skin.expert.mobile.model.network.NetworkResult
import skin.expert.mobile.model.network.NetworkService
import skin.expert.mobile.model.network.error.http.*
import skin.expert.mobile.model.network.error.server.UnhandledServerError
import skin.expert.mobile.model.network.response.base.ServerResponse
import skin.expert.mobile.model.network.wrapError

class ErrorHandlerTest {

    @Before
    fun initRetrofitInstance() {
        mockRetrofitInstanceInNetworkService()
    }

    @Test
    fun wrapError_200codeERRORstatus_UnhandledServerError() {
        runBlocking {
            val errorsMessagesList = listOf("Mocked errors messages")
            val serverResponse = wrapError {
                val serverResponse = ServerResponse<Any>(
                    "ERROR",
                    null,
                    errorsMessagesList
                )
                mockSuccessResponse(200, serverResponse)
            }

            assert(serverResponse is NetworkResult.Failure)

            val serverFailure = serverResponse as NetworkResult.Failure
            assert(serverFailure.value is UnhandledServerError)
            assert((serverFailure.value as UnhandledServerError).message == errorsMessagesList.toString())
        }
    }

    @Test
    fun wrapError_404code_NothingFoundError() {
        runBlocking {
            mockRetrofitInstanceInNetworkService()

            val serverResponse = wrapError {
                mockErrorResponse<Any>(400, RESPONSE_404)
            }

            assert(serverResponse is NetworkResult.Failure)

            val serverFailure = serverResponse as NetworkResult.Failure
            assert(serverFailure.value is NothingFountError)
        }
    }

    @Test
    fun wrapError_401code_UnauthenticatedError() {
        runBlocking {
            val serverResponse = wrapError {
                mockErrorResponse<Any>(500, RESPONSE_401)
            }

            assert(serverResponse is NetworkResult.Failure)

            val serverFailure = serverResponse as NetworkResult.Failure
            assert(serverFailure.value is UnauthenticatedError)
            assert((serverFailure.value as UnauthenticatedError).message == listOf("Your request was made with invalid credentials.").toString())
        }
    }

    @Test
    fun wrapError_400code_BadRequestError() {
        runBlocking {
            val serverResponse = wrapError {
                mockErrorResponse<Any>(400, "")
            }

            assert(serverResponse is NetworkResult.Failure)

            val serverFailure = serverResponse as NetworkResult.Failure
            assert(serverFailure.value is BadRequestError)
        }
    }

    @Test
    fun wrapError_500code_ServerError() {
        runBlocking {
            val serverResponse = wrapError {
                mockErrorResponse<Any>(500, "")
            }

            assert(serverResponse is NetworkResult.Failure)

            val serverFailure = serverResponse as NetworkResult.Failure
            assert(serverFailure.value is ServerError)
        }
    }

    @Test
    fun wrapError_407code_ServerError() {
        runBlocking {
            val serverResponse = wrapError {
                mockErrorResponse<Any>(407, "")
            }

            assert(serverResponse is NetworkResult.Failure)

            val serverFailure = serverResponse as NetworkResult.Failure
            assert(serverFailure.value is ClientError)
        }
    }

    @Test
    fun wrapError_503code_ServerError() {
        runBlocking {
            val serverResponse = wrapError {
                mockErrorResponse<Any>(503, "")
            }

            assert(serverResponse is NetworkResult.Failure)

            val serverFailure = serverResponse as NetworkResult.Failure
            assert(serverFailure.value is ServerError)
        }
    }


    private fun <T> mockErrorResponse(code: Int, content: String) : Response<ServerResponse<T>> {
        return Response.error(
            code,
            ResponseBody.create(MediaType.parse("application/json"), content)
        )
    }

    private fun <T> mockSuccessResponse(code: Int, data: ServerResponse<T>) : Response<ServerResponse<T>> {
        return Response.success(code, data)
    }

    private fun mockRetrofitInstanceInNetworkService() {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://localhost/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        NetworkService.retrofit = retrofit
    }

    companion object {
        const val RESPONSE_404 : String =
            """{ "status": "ERROR", "error": "404" }"""
        const val RESPONSE_401 : String =
            """{
                "status": "ERROR",
                "errors": [
                    "Your request was made with invalid credentials."
                ],
                "error_code": "401",
                "error_id": 253227
            }"""

    }
}
package skin.expert.mobile

import org.junit.Test
import skin.expert.mobile.model.data.Client
import skin.expert.mobile.model.data.Photo
import skin.expert.mobile.model.data.Point
import skin.expert.mobile.model.validation.validateClient
import skin.expert.mobile.model.validation.validatePhoto
import skin.expert.mobile.model.validation.validatePoint

class DataValidationTest {

    @Test
    fun validateClient_correctClient_trueValidate() {
        val client = Client(
            id = 0,
            name = "ferf",
            last_name = "ferf",
            middle_name = "fref",
            address = "some",
            VIN = "344356254",
            defects_description = null,
            diagnoses_description = null,
            diagnost_name = null,
            mechanical_description = null,
            mechanical_name = null,
            model = null,
            number = null,
            phone = null,
            photo_path = null,
            photoList = null,
            problem = null,
            reciever_name = null
        )

        val result = validateClient(client)
        assert(result)
    }

    @Test
    fun validateClient_incorrectClient_falseValidate() {
        val client = Client(
            id = 0,
            name = null,
            last_name = "ferf",
            middle_name = "fref",
            address = "some",
            VIN = "344356254",
            defects_description = null,
            diagnoses_description = null,
            diagnost_name = null,
            mechanical_description = null,
            mechanical_name = null,
            model = null,
            number = null,
            phone = null,
            photo_path = null,
            photoList = null,
            problem = null,
            reciever_name = null
        )

        val result = validateClient(client)
        assert(!result)
    }

    @Test
    fun validateClient_nullClient_falseValidate() {
        val result = validateClient(null)
        assert(!result)
    }

    @Test
    fun validateClient_correctPhoto_trueValidate() {
        val photo = Photo(
            id = 0,
            notes = null,
            path = "some path",
            points = null
        )

        val result = validatePhoto(photo)
        assert(result)
    }

    @Test
    fun validateClient_incorrectPhoto_falseValidate() {
        val photo = Photo(
            id = 0,
            notes = null,
            path = null,
            points = null
        )

        val result = validatePhoto(photo)
        assert(!result)
    }

    @Test
    fun validateClient_nullPhoto_falseValidate() {
        val result = validatePhoto(null)
        assert(!result)
    }

    @Test
    fun validateClient_correctPoint_trueValidate() {
        val point = Point(
            id = 0,
            x = 23,
            y = 43,
            description = null
        )

        val result = validatePoint(point)
        assert(result)
    }

    @Test
    fun validateClient_incorrectPoint_falseValidate() {
        val point = Point(
            id = 0,
            x = 23,
            y = null,
            description = null
        )

        val result = validatePoint(point)
        assert(!result)
    }

    @Test
    fun validateClient_nullPoint_falseValidate() {
        val result = validatePoint(null)
        assert(!result)
    }

}
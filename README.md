# Лабораторные работы по предмету ТРПО

## Лабораторная работа #1
### Определить основные процессы работы любого предприятия: определить бизнес-процессы и промоделировать их на языке IDEF0, дополнить IDEF3 & DFD. Это фактически фаза обследования будущего обьекта для автоматизации.

Короткая версия IDEF0:

![picture](idef0_short.png)

Полная версия IDEF0:

![picture](idef0_extended.png)

DFD:

![picture](dfd.png)

IDEF3:

![picture](idef3.png)

## Лабораторная работа #2
### Разработать требования к изделию = это не что иное как ТЗ

см. lab2.md

## Лабораторная работа #3
### Описать в табличном и текстовом спец виде USE CASE (это не UML) для реализации требований. Начальная формализация.

см. lab3.md

## Лабораторная работа #4

Modules diagram:

![picture](modules.png)

Classes diagram:

![picture](classes.png)

Sequence diagram для просмотра списка клиентов:

![picture](sequence_client_list.png)

Sequence diagram для редактирования записи:

![picture](sequence_edit_client.png)

DDL:

``` sql

CREATE DATABASE IF NOT EXISTS carPro;

USE carPro;

CREATE TABLE Client(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(24) NOT NULL,
    last_name VARCHAR(24) NOT NULL,
    middle_name VARCHAR(24) NOT NULL,
    phone VARCHAR(24) NOT NULL,
    address TEXT NOT NULL,
    model TEXT NOT NULL,
    number VARCHAR(24) NOT NULL,
    VIN VARCHAR(24) NOT NULL,
    photo_path TEXT,
    reciever_name TEXT NOT NULL,
    diagnost_name TEXT NOT NULL,
    mechanical_name TEXT NOT NULL,
    defects_description TEXT NOT NULL,
    problem TEXT NOT NULL,
    diagnoses_description TEXT,
    mechanical_description TEXT,
    client_recall TEXT
);

CREATE TABLE Photo(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    path TEXT NOT NULL,
    notes TEXT,
    FOREIGN KEY (client_id) REFERENCES Client(id)
);

CREATE TABLE Point(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    x INT NOT NULL,
    y INT NOT NULL,
    description TEXT,
    FOREIGN KEY (photo_id) REFERENCES Photo(id)
);

```

## Лабораторная работа #5

Файлы с юнит тестами: DataValidationText.kt, ErrorHndlerTest.kt